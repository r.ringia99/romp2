﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Romp2
{
    public class Joint : SkeletonObject
    {
        public Point Location; // Position of the Joint
        public List<SkeletonObject> SkeletonObjects = new List<SkeletonObject>(); // All lines with de begin position of this joint
        public string Name { get; set; } // Name of the joint
        private Pen Color; // The color
        private readonly int Width = 20;
        private readonly int Height = 20;
        private bool CanIDraw = true; // Under construction

        // Create standard joint
        public Joint(string name)
        {
            Name = name;
            Color = new Pen(System.Drawing.Color.Red);
        }

        // Create a joint with a static location
        public Joint(string name, Point location)
        {
            Name = name;
            Color = new Pen(System.Drawing.Color.Red);
            Location = location;
        }


        // Voeg een lijn toe aan een list
        public void AddSkeletonObject(SkeletonObject skeletonBase)
        {
            SkeletonObjects.Add(skeletonBase);
        }

        // Get the location for the line in the joint
        public Point GetLineLocation()
        {
            return new Point(Location.X + (Width / 2), Location.Y + (Height / 2));
        }

        public override void Update(Point? p = null)
        {
            if (p != null) // if there is a new location it needs to change. 
            {
                Location = new Point(p.Value.X - (Width / 2), p.Value.Y - (Height / 2));
                //CanIDraw = true;
            }

            // Update the other SkeletonObjects that are connected on this object
            foreach (SkeletonObject skeletonObject in SkeletonObjects) skeletonObject.Update(GetLineLocation());
        }

        public override void Draw(Graphics graphics)
        {
            //if (CanIDraw)
            //{
            //    Console.WriteLine("Joint draw (" + Name + ")");
            //    graphics.DrawEllipse(Color, Location.X, Location.Y, Width, Height);
            //    CanIDraw = false;
            //}

            Console.WriteLine("Joint draw (" + Name + ")");
            // Draw the joint on the panel
            graphics.DrawEllipse(Color, Location.X, Location.Y, Width, Height);
            // Call the other draw function of the objects that are connected
            foreach (SkeletonObject skeletonObject in SkeletonObjects) skeletonObject.Draw(graphics);
        }
    }
}
