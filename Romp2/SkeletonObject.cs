﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Romp2
{
    public abstract class SkeletonObject
    {
        // The update function will calculate where the line or the joint will stand with its new location. 
        // The Point is for updating a location that needs to be the same location from the update where its called from.
        abstract public void Update(Point? p = null);
        abstract public void Draw(Graphics graphics);
    }
}
