﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Romp2
{
    public partial class Form1 : Form
    {
        SkeletonControl skeletonControl;
        public Form1()
        {
            InitializeComponent();

            this.DoubleBuffered = true;

            Panel trackBarPanel = new Panel();
            trackBarPanel.Location = new Point(600, 0);

            Joint beginJoint = new Joint("BeginJoint", new Point(250, 150));
            skeletonControl = new SkeletonControl(beginJoint, trackBarPanel, new Point(10, 10), new Size(700, 400));

            // LICHAAM
            Line lichaam = new Line("Lichaam", skeletonControl.MainSkeletonObject, 100, 270);
            beginJoint.AddSkeletonObject(lichaam);
            skeletonControl.AddTrackBarToLine(lichaam);

            // BOVEN ARM

            Joint schouder = new Joint("Schouder");
            lichaam.SetEndPoint(schouder);

            Line bovenArmRechts = new Line("Boven arm rechts", schouder, 100, 0);
            schouder.AddSkeletonObject(bovenArmRechts);
            skeletonControl.AddTrackBarToLine(bovenArmRechts);

            Line bovenArmLinks = new Line("Boven arm links", schouder, 100, 180);
            schouder.AddSkeletonObject(bovenArmLinks);
            skeletonControl.AddTrackBarToLine(bovenArmLinks);

            Joint bovenArmRechtsJoint = new Joint("Boven arm rechts joint");
            bovenArmRechts.SetEndPoint(bovenArmRechtsJoint);

            Joint bovenArmLinksJoint = new Joint("Boven arm links joint");
            bovenArmLinks.SetEndPoint(bovenArmLinksJoint);


            // ONDER ARM
            Line onderArmRechts = new Line("Onder arm rechts", bovenArmRechtsJoint, 100, 45);
            bovenArmRechtsJoint.AddSkeletonObject(onderArmRechts);
            skeletonControl.AddTrackBarToLine(onderArmRechts);


            Line onderArmLinks = new Line("Onder arm links", bovenArmLinksJoint, 100, 135);
            bovenArmLinksJoint.AddSkeletonObject(onderArmLinks);
            skeletonControl.AddTrackBarToLine(onderArmLinks);

            // BENEN
            Line bovenBeenRechts = new Line("Boven been rechts", beginJoint, 100, 55);
            beginJoint.AddSkeletonObject(bovenBeenRechts);
            skeletonControl.AddTrackBarToLine(bovenBeenRechts);

            Line bovenBeenLinks = new Line("Boven been links", beginJoint, 100, 125);
            beginJoint.AddSkeletonObject(bovenBeenLinks);
            skeletonControl.AddTrackBarToLine(bovenBeenLinks);

            Joint knieRechts = new Joint("Knie rechts");
            bovenBeenRechts.SetEndPoint(knieRechts);

            Joint knieLinks = new Joint("Knie links");
            bovenBeenLinks.SetEndPoint(knieLinks);

            Line onderBeenRechts = new Line("Onder been rechts", knieRechts, 100, 90);
            knieRechts.AddSkeletonObject(onderBeenRechts);
            skeletonControl.AddTrackBarToLine(onderBeenRechts);

            Line onderBeenLinks = new Line("Onder been links", knieLinks, 100, 90);
            knieLinks.AddSkeletonObject(onderBeenLinks);
            skeletonControl.AddTrackBarToLine(onderBeenLinks);


            skeletonControl.Update();
            Controls.Add(skeletonControl);
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
