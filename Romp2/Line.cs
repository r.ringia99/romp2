﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Romp2
{
    public class Line : SkeletonObject
    {
        private Pen Color;
        public SkeletonObject StartSkeletonObject { get; set; } // The start skeleton object for this object.
        public SkeletonObject EndSkeletonObject { get; set; } // The end skeleton object for this object. This is needed for updating and drawing the next objects.
        protected double Angle; // The angle of the line
        protected Point StartLocation { get; set; } // The start location of the line
        protected Point EndLocation { get; set; } // the end location of the line
        private int Lenght = 100; // The lenght of the line
        public string Name { get; set; }
        

        // Normal line
        public Line(string name, SkeletonObject skeletonObject, int Length, int Deg)
        {
            Name = name;
            StartSkeletonObject = skeletonObject;
            Lenght = Length;
            Angle = GetAngle(Deg);
            Color = new Pen(System.Drawing.Color.Red);
        }
        
        // Get the angle from deg
        private double GetAngle(int Deg)
        {
            return (Deg) * Math.PI / 180;
        }

        // Get deg trom the angle
        public double GetDeg()
        {
            return (Angle) / Math.PI * 180;
        }

        // Set the angle from deg
        public void SetAngle(int Deg)
        {
            Angle = GetAngle(Deg);
        }

        // The the start skeleton object
        public void SetStartPoint(SkeletonObject skeletonObject)
        {
            StartSkeletonObject = skeletonObject;
        }

        // Set the end skeleton object of the line
        public void SetEndPoint(SkeletonObject skeletonObject)
        {
            EndSkeletonObject = skeletonObject;
        }


        public override void Update(Point? p = null)
        {
            Console.WriteLine("Line update");
            StartLocation = p ?? StartLocation; // If the given point is null it will keeps its old startpoint. Otherwise it will get a new start point.
            // Create the end location
            int x2 = (int)(StartLocation.X + Lenght * Math.Cos(Angle));
            int y2 = (int)(StartLocation.Y + Lenght * Math.Sin(Angle));
            EndLocation = new Point(x2, y2);
            // If there is an end location it will update
            if (EndSkeletonObject != null) EndSkeletonObject.Update(EndLocation);
        }

        public override void Draw(Graphics graphics)
        {
            Console.WriteLine("Line draw (" + Name + ")");
            // Draw the line on the panel
            graphics.DrawLine(Color, StartLocation, EndLocation);
            // If there is an end location it will call his draw function
            if (EndSkeletonObject != null) EndSkeletonObject.Draw(graphics);
        }
        
    }
}
