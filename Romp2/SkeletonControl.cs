﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Romp2
{
    public class SkeletonControl : Control
    { 
        public SkeletonObject MainSkeletonObject;
        public Panel TrackBarPanel;

        public SkeletonControl(SkeletonObject MainSkeletonObject, Panel TrackBarPanel, Point Location, Size size)
        {
            this.MainSkeletonObject = MainSkeletonObject;
            this.TrackBarPanel = TrackBarPanel;
            this.Location = Location;
            this.Size = new Size(700, 400);

            Controls.Add(TrackBarPanel); // Add the trackbar panel to this control
        }

        // Paint the main skeleton object
        protected override void OnPaint(PaintEventArgs e)
        {
            MainSkeletonObject.Draw(e.Graphics);
        }

        // Update the main skeleton object
        public new void Update()
        {
            MainSkeletonObject.Update();
        }

        // Add the line to a trackbar
        public void AddTrackBarToLine(Line line)
        {
            LineTrackBar lineTrackBar = new LineTrackBar(line, 0, 360, this); // Create a LineTrackbar from the given line
            var trackBarPanelList = TrackBarPanel.Controls.OfType<LineTrackBar>(); // Check how many trackbar there are in the trackbar panel. For calculating the position.
            lineTrackBar.Location = new Point(0, trackBarPanelList.Count() * (lineTrackBar.Height + 10)); // Set the location by calculating the location. This will need the count of the trackbars in the panel and the height of this trackbar
            TrackBarPanel.Controls.Add(lineTrackBar); // Add him to the trackbarpanel
            TrackBarPanel.Size = new Size(lineTrackBar.Width, TrackBarPanel.Size.Height + lineTrackBar.Height + 10); // change the size of the trackbarpanel
        }
        
    }
}
