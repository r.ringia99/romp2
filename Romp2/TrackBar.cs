﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Romp2
{
    public class LineTrackBar : TrackBar
    {
        private Line Line;
        private SkeletonControl SkeletonControl;
        public LineTrackBar(Line line, int Min, int Max, SkeletonControl skeletonControl)
        {
            Line = line;
            Maximum = Max;
            Minimum = Min;
            Value = (int)Line.GetDeg();
            SkeletonControl = skeletonControl;
            ValueChanged += TrackBar_ValueChanged;
        }

        private void TrackBar_ValueChanged(object sender, EventArgs e)
        {
            Line.SetAngle(Value);
            Line.Update();
            SkeletonControl.Invalidate();
        }
    }
}
